<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/persons', function (Request $request, Response $response, $args) {
    $q = $request->getQueryParam('q');
    // Render index view
    $tplVars = [];
    try{
        if(empty($q)){
            $stmt = $this->db->prepare('SELECT * FROM person ORDER BY last_name');
        }
        else{
            $stmt = $this->db->prepare('SELECT * FROM person WHERE last_name ILIKE :query OR first_name ILIKE :query OR nickname ILIKE :query ORDER BY last_name');
            $stmt->bindValue(':query', $q . '%');
        }
        $stmt->execute();
    } catch (Exception $e){
        $this->logger->error($e->getMessage());
        //file($e->getMessage());
        die($e->getMessage());
    }

    $stmt->execute();
    $tplVars['people'] = $stmt->fetchAll();
    return $this->view->render($response, 'persons.latte', $tplVars);
})->setName('personList');

$app->get('/add-person', function (Request $request, Response $response, $args) {
    // Render index view
    $tplVars = [];
    return $this->view->render($response, 'add-person.latte', $tplVars);
})->setName('addPerson');

$app->post('/add-person', function (Request $request, Response $response, $args) {
    //read POST data
    $data = $request->getParsedBody();
    if(!empty($data['fn']) && !empty($data['ln']) && !empty($data['nn'])){
        try {
            $stmt = $this->db->prepare('INSERT INTO person(gender, height, first_name, last_name, nickname) VALUES (:g, :h, :fn, :ln, :nn)');
            $g = empty($data['g']) ? null : $data['g'];
            $h = empty($data['h']) ? null : $data['h'];
            $stmt->bindValue(':g', $g);
            $stmt->bindValue(':h', $h);
            $stmt->bindValue(':fn', $data['fn']);
            $stmt->bindValue(':ln', $data['ln']);
            $stmt->bindValue(':nn', $data['nn']);
            $stmt->execute();
            return $response->withHeader('Location', $this->router->pathFor('personList'));
        } catch(Exception $e) {
            if($e->getCode() == 23505) {
                $tplVars['error'] = 'The record already exists';
                $tplVars['form'] = $data;
                return $this->view->render($response, 'add-person.latte');
            } else {
                $this->logger->error($e->getMessage());
                die($e->getMessage());
            }
        }
    } else {
        die('Specify all required values');
    }

});
    //$input = $request->getParsedBody();

    //log
    /*$this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');*/
